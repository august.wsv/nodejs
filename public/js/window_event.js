if (!navigator.onLine) {
    alert('Không có kết nối Internet, các tài nguyên bên thứ 3 sử dụng không thể tải được!');
}
function checkInternetConnection() {
    alert(navigator.onLine ? 'Đã kết nối Internet!' : 'Không có kết nối Internet!');
}
window.addEventListener('online', checkInternetConnection);
window.addEventListener('offline', checkInternetConnection);

window.addEventListener('pageshow', function (event) {
    if (event.persisted) { location.reload(); }
});