function countdown(stringTargetDate) {
    const targetDate = new Date(stringTargetDate).getTime();
    const now = new Date().getTime();
    const timeRemaining = targetDate - now;

    if (timeRemaining > 0) {
        const days = Math.floor(timeRemaining / (1000 * 60 * 60 * 24));
        const hours = Math.floor((timeRemaining % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60)).toString().padStart(2, '0');
        const minutes = Math.floor((timeRemaining % (1000 * 60 * 60)) / (1000 * 60)).toString().padStart(2, '0');
        const seconds = Math.floor((timeRemaining % (1000 * 60)) / 1000).toString().padStart(2, '0');

        document.querySelector("#countdown p").innerHTML = `${days} ngày ${hours} giờ ${minutes} phút ${seconds} giây`;

        requestAnimationFrame(() => countdown(stringTargetDate));
    } else {
        document.querySelector("#countdown p").innerHTML = "Đã hết thời gian!";
    }
}