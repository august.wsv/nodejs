import { asset } from '../helpers/viewHelper.mjs';
import { route } from '../helpers/routerHelper.mjs';
class RFTMiddleware {
    hander(req, res, next) {
        res.locals.asset = asset;
        res.locals.route = route;
        next();
    };
}

export default new RFTMiddleware();