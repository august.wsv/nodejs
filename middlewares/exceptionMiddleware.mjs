import log from '../setup/log.mjs';
class ExceptionMiddleware {
    notFoundHandler(req, res, next) {
        const fullUrl = req.protocol + '://' + req.get('host') + req.originalUrl;
        const content = `${new Date()} at ${fullUrl}: This URL does not exist.`
        log.errorLog(content);
        return res.render('default/404error');
    }
    errorHandler(err, req, res, next) {
        const fullUrl = req.protocol + '://' + req.get('host') + req.originalUrl;
        const content = `${new Date()} at ${fullUrl}: Server error!!!\n${err}`
        log.errorLog(content);
        return res.render('default/500error');
    }
}

export default new ExceptionMiddleware();