import jwt from 'jsonwebtoken';
class AuthMiddleware {
    requireLogin(req, res, next) {
        let isTokenValid = false;
        const token = req.session.token;
        if (token) {
            const secretKey = process.env.JWT_SECRET;
            try {
                jwt.verify(token, secretKey);
                isTokenValid = true;
            } catch (error) { }
        }
        if (!isTokenValid) {
            return res.render('lam-viec-voi-api/dang-nhap');
        }
        return next();
    };
}

export default new AuthMiddleware();