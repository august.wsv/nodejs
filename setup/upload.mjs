import multer from 'multer';
import fs from 'fs';
class Upload {
    #storage(destination) {
        let customDestination = `public/share/${destination}`;
        if (!fs.existsSync(customDestination)) { fs.mkdirSync(customDestination, { recursive: true }); }
        return multer.diskStorage({
            destination: (req, file, cb) => {
                cb(null, customDestination);
            },
            filename: (req, file, cb) => {
                cb(null, `${Date.now()}-${Math.round(Math.random() * 1E9)}.${file.originalname.split('.').pop()}`);
            },
        });
    }

    #limits(fileSize) {
        return {
            fileSize: fileSize * 1024 * 1024,
        };
    }

    #parseSizeString(sizeString) {
        const parts = sizeString.match(/(\d+)\s*([KMGT]B)/);
        if (!parts) {
            throw new Error('Invalid size string format');
        }
    
        const size = parseInt(parts[1]);
        const unit = parts[2];
    
        switch (unit) {
            case 'KB':
                return size * 1024;
            case 'MB':
                return size * 1024 * 1024;
            case 'GB':
                return size * 1024 * 1024 * 1024;
            case 'TB':
                return size * 1024 * 1024 * 1024 * 1024;
            default:
                throw new Error('Invalid size unit');
        }
    }

    fileFilter(req, file, cb, allowedMimes) {
        if (allowedMimes.includes(file.mimetype)) {
            cb(null, true);
        } else {
            cb(null, false); 
        }
    }

    allField({ destination, fieldsData, fileSize = Infinity, fileFilters }) {
        return multer({
            storage: this.#storage(destination),
            limits: this.#limits(this.#parseSizeString(fileSize)),
            fileFilter: (req, file, cb) => {
                const filterFunction = fileFilters[file.fieldname];
                if (filterFunction) {
                    filterFunction(req, file, cb);
                } else {
                    cb(null, true);
                }
            },
        }).fields(fieldsData)
    }
}

export default new Upload();