import UserController from '../controllers/userController.mjs';
import IndexController from '../controllers/indexController.mjs';
import AuthMiddleware from '../middlewares/authMiddleware.mjs';
import { register } from '../helpers/routerHelper.mjs';

export default (express) => {
    const router = express.Router();
    router.post(
        register('/dang-nhap', 'dangNhap'),
        UserController.login
    );
    const authRouter = express.Router();
    authRouter.use(AuthMiddleware.requireLogin);
    authRouter.get(
        register('/lay-danh-sach', 'layDanhSach'),
        UserController.renderUserList
    );
    authRouter.get(
        register('/xuat-file-excel', 'xuatFile'),
        UserController.exportExcelUserlist
    );
    router.get(register('/', 'home'), IndexController.defaultX);
    router.get('/dem-nguoc', IndexController.renderCountdown);
    router.get('/page1', IndexController.renderI);
    router.get('/page2', IndexController.renderII);

    router.get('/check', IndexController.renderCheck);
    router.get(register('/test/:id/and/:name', 'testRoute'), IndexController.getParamsFromURL);
    router.get('/request', IndexController.requestSession);
    router.post('/upload-file', IndexController.uploadFile)
    router.use('/auth', authRouter);
    return router;
};