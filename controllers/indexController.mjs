import log from '../setup/log.mjs';
import upload from '../setup/upload.mjs';
import { bindMethodsToThis } from '../helpers/binderHelper.mjs';

class IndexController {
    constructor(name) {
        bindMethodsToThis(this);
        this.name = name;
    }

    defaultX(req, res) {
        try {
            return res.send(`《|defaultX page|》${this.name}`);
        } catch (error) {
            log.debugLog(error);
            return res.render('default/500error');
        }
    };

    async renderCountdown(req, res) {
        try {
            const pageTitle = 'Đồng hồ đếm ngược';
            const endTime = '2025-04-25T17:15:00'
            return res.render('lam-viec-voi-view/my-countdown', { pageTitle, endTime });
        } catch (error) {
            log.debugLog(error);
            return res.render('default/500error');
        }
    };

    async renderI(req, res) {
        let fakeUsers = [
            {
                "name": "John Doe",
                "email": "john.doe@example.com",
                "age": 30
            },
            {
                "name": "Jane Smith",
                "email": "jane.smith@example.com",
                "age": 28
            },
            {
                "name": "Bob Johnson",
                "email": "bob.johnson@example.com",
                "age": 35
            }
        ]
        const users = req.session.users ?? fakeUsers;
        try {
            return res.render('lam-viec-voi-view/page1', { users });
        } catch (error) {
            log.debugLog(error);
            return res.render('default/500error');
        }
    };

    async renderII(req, res) {
        try {
            return res.render('lam-viec-voi-view/page2', {
                pageTitle: 'TEST### PAGE2',
                endTime: '2100-01-01T17:15:00',
            });
        } catch (error) {
            log.debugLog(error);
            return res.render('default/500error');
        }
    };

    async getParamsFromURL(req, res) {
        try {
            return res.json(req.params);
        } catch (error) {
            log.debugLog(error);
            return res.render('default/500error');
        }
    };

    async requestSession(req, res) {
        return res.json(req.session);
    };

    async renderCheck(req, res) {
        try {
            return res.render('test-thu/check');
        } catch (error) {
            log.debugLog(error);
            return res.render('default/500error');
        }
    };

    async uploadFile(req, res) {
        try {
            let error = await new Promise((resolve, reject) => {
                upload.allField({
                    destination: 'uploads',
                    fileSize: '500MB',
                    fieldsData: [
                        { name: 'files[]', maxCount: 3 }, 
                        { name: 'file', maxCount: 1 },
                        { name: 'any', maxCount: 1 }
                    ],
                    fileFilters: {
                        'files[]': (req, file, cb) => upload.fileFilter(req, file, cb, ['image/jpeg', 'image/png', 'image/gif']),
                        'file': (req, file, cb) => upload.fileFilter(req, file, cb, ['video/mp4', 'video/mpeg']),
                    }                                       
                })(req, res, (err) => {
                    if (err) { reject(err); } else { resolve(null); } 
                });
            });
            console.log(req.body.token)
            return !error ? res.send('File đã được tải lên.') : (() => { throw new Error(error); })();
        } catch (error) {
            log.debugLog(error);
            return res.render('default/500error');
        }

    };
}

export default new IndexController('something');