import axios from 'axios';
import log from '../setup/log.mjs';
import exceljs from 'exceljs';
import { bindMethodsToThis } from '../helpers/binderHelper.mjs';
class UserController {
    constructor() {
        bindMethodsToThis(this);
        this.apiServerDomain = 'http://127.0.0.1:8000';
        this.jsonData = null;
    }

    async login(req, res) {
        let { email, password } = req.body;
        if (req.session.token) { return res.json({ status: true }); }
        let data = { email, password };
        let config = {
            method: 'post',
            maxBodyLength: Infinity,
            url: `${this.apiServerDomain}/api/v1/login`,
            headers: {
                'Content-Type': 'application/json'
            },
            data: data
        };
        axios.request(config)
            .then((response) => {
                req.session.token = response.data.meta.token;
                return res.json({ status: true });
            })
            .catch((error) => {
                try {
                    return res.json({ status: false, error: error.response.data });
                } catch (catchError) {
                    return res.json({ status: false, error: error });
                }
            });
    };

    async renderUserList(req, res) {
        let config = {
            method: 'get',
            maxBodyLength: Infinity,
            url: `${this.apiServerDomain}/api/v1/users`,
            headers: {
                'Authorization': `Bearer ${req.session.token}`
            }
        };
        let pageTitle = 'Danh sách';
        axios.request(config)
            .then((response) => {
                let users = response.data.data.users;
                this.jsonData = users;
                return res.render('lam-viec-voi-api/danh-sach-nhan-vien', { pageTitle, users });
            })
            .catch((error) => {
                res.status(500).send("Lỗi khi gửi yêu cầu!");
            });
    }

    async exportExcelUserlist(req, res) {
        try {
            const workbook = new exceljs.Workbook();
            const worksheet = workbook.addWorksheet('Sheet 1');
            const redFont = { color: { argb: 'FF0000' } };
            const greenFont = { color: { argb: 'FF00FF' } };
            const jsonData = this.jsonData;
            const filename = 'danh_sach_thong_tin_nhan_vien';
            worksheet.addRow(['Tên', 'Giới tính', 'Số điện thoại', 'Email', 'Địa chỉ']);
            jsonData.forEach((row) => {
                const dataRow = worksheet.addRow([row.name, row.sex == 0 ? 'Nam' : 'Nữ', row.phone, row.email, row.address]);
                dataRow.getCell(2).font = row.sex === 0 ? redFont : greenFont;
            });
            worksheet.getColumn(1).width = 25;
            worksheet.getColumn(3).width = 12;
            worksheet.getColumn(4).width = 30;
            worksheet.getColumn(5).width = 60;
            const excelBuffer = await workbook.xlsx.writeBuffer();
            res.setHeader('Content-Type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            res.setHeader('Content-Disposition', `attachment; filename=${filename}.xlsx`);
            res.send(excelBuffer);
        } catch (error) {
            log.debugLog(error);
            return res.render('default/500error');
        }
    }
}

export default new UserController();