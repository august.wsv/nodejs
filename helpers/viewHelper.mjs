function asset(path) {
    const domain = process.env.NODE_URL;
    return domain + path;
}

export { asset };